\begin{thebibliography}{10}

\bibitem{gunasekaran2011}
Angappa Gunasekaran, Bharatendra~K Rai, and Michael Griffin.
\newblock Resilience and competitiveness of small and medium size enterprises:
  an empirical research.
\newblock {\em International journal of production research}, 2011.

\bibitem{elmaraghy2012}
Hoda ElMaraghy, T~AlGeddawy, A~Azab, and Waguih ElMaraghy.
\newblock Change in manufacturing--research and industrial challenges.
\newblock {\em Enabling manufacturing competitiveness and economic
  sustainability}, pages 2--9, 2012.

\bibitem{christensen2013}
Clayton Christensen.
\newblock {\em The innovator's dilemma: when new technologies cause great firms
  to fail}.
\newblock Harvard Business Review Press, 2013.

\bibitem{azab2013}
Ahmed Azab, H~ElMaraghy, P~Nyhuis, J~Pachow-Frauenhofer, and M~Schmidt.
\newblock Mechanics of change: A framework to reconfigure manufacturing
  systems.
\newblock {\em CIRP Journal of Manufacturing Science and Technology},
  6(2):110--119, 2013.

\bibitem{koren2017}
Yoram Koren, Wencai Wang, and Xi~Gu.
\newblock Value creation through design for scalability of reconfigurable
  manufacturing systems.
\newblock {\em International Journal of Production Research}, 55(5):1227--1242,
  2017.

\bibitem{tolio2010}
T~Tolio, Darek Ceglarek, HA~ElMaraghy, A~Fischer, SJ~Hu, L~Laperri{\`e}re,
  Stephen~T Newman, and J{\'o}zsef V{\'a}ncza.
\newblock Species—co-evolution of products, processes and production systems.
\newblock {\em CIRP Annals-Manufacturing Technology}, 59(2):672--693, 2010.

\bibitem{rosio2012}
Carin R{\"o}si{\"o}.
\newblock Considering reconfigurability characteristics in production system
  design.
\newblock {\em Enabling Manufacturing Competitiveness and Economic
  Sustainability}, pages 57--62, 2012.

\bibitem{tolio2008}
Tullio Tolio.
\newblock {\em Design of flexible production systems}.
\newblock Springer, 2008.

\bibitem{rudtsch2013}
Vinzent Rudtsch, Frank Bauer, and J{\"u}rgen Gausemeier.
\newblock Approach for the conceptual design validation of production systems
  using automated simulation-model generation.
\newblock {\em Procedia Computer Science}, 2013.

\bibitem{tolio2013}
Tullio Tolio, Marco Sacco, Walter Terkaj, and Marcello Urgo.
\newblock Virtual factory: An integrated framework for manufacturing systems
  design and analysis.
\newblock {\em Procedia CIRP}, 7:25--30, 2013.

\bibitem{colledani2009}
Marcello Colledani, Walter Terkaj, and Tullio Tolio.
\newblock Product-process-system information formalization.
\newblock In {\em Design of Flexible Production Systems}, pages 63--86.
  Springer, 2009.

\end{thebibliography}
